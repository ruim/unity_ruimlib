# Unity_RuimLib

A set of tools for Unity that I use regularly on my projects

## Includes handy tools to work with:
## AI
    - Based on the book "Programming AI by Example" by Mat Buckland and addapted to Unity
    - Finite State Machines
    - Steering Behaviours

## Color
    - Handy color utils to work with colors and generate color palettes using color harmonies

## Cam
    - Scaling 2d cameras
    - Calculating camera bounds in world space
    - Creating quads that adjust to camera size (useful to create backgrounds)

## Math
    - Angle struct to work make it easier to work with degrees or radians
    - Range struct to define ranges of numbers
    - Easing functions
    - An implementation of Perlin Noise in 1D, 2D and 3D (the default Unity implementation only supports 2D noise)
    - Utils to work with probabilities 

## UI
    - Set of utilities to create UI elements including prefabs to speed up UI creation
    - A manager to show/hide different screens with optional animations

## Utils: some generic utilities
    - Singletons for MonoBehaviours
    - Templated class to create pools of MonoBehaviours
    - LayerUtils to check if a certain object belongs in a LayerMask (much faster than comparing layer or tag names)
    - ScreenshotCapture utility (usefull when creating AppStore screenshots)

## Verlet
    - A verlet physics implementation based on this paper: https://www.cs.cmu.edu/afs/cs/academic/class/15462-s13/www/lec_slides/Jakobsen.pdf
    - Verlet particles in 3D
    - Verlet springs in 2D

## Getting started
Clone the repo and add the folder into the Assets folder in your Unity project

## Roadmap
Add examples and better documentation

## Author
Created by Rui Madeira

## License
MIT license https://opensource.org/licenses/MIT

## Project status
I will maintain and update this project as I make changes/additions to it based on my projects. 


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Ruim.Verlet
{
    public class VerletSpring2D : MonoBehaviour
    {

        public VerletParticle3D partA, partB;
        public float restDistance;
        public float restForce = 1f;

        public void SetCurrDistAsRest(){
            restDistance = (partA.position - partB.position).magnitude;
        }

        public void Init(VerletParticle3D partA, VerletParticle3D partB){
            this.partA = partA;
            this.partB = partB;
            SetCurrDistAsRest();
        }

        public void Init(VerletParticle3D partA, VerletParticle3D partB, float restDistance){
            this.partA = partA;
            this.partB = partB;
            this.restDistance = restDistance;
        }

        // Use this for initialization
       /* void Awake()
        {

        }*/

        // Update is called once per frame
        void Update()
        {

            if(Time.deltaTime < Mathf.Epsilon){ //paused
                return;
            }

            var posA = partA.position;
            var posB = partB.position;
            float dx = posB.x - posA.x;
            float dy = posB.y - posA.y;
            float deltaLength = Mathf.Sqrt(dx * dx + dy * dy);
            if(deltaLength > Mathf.Epsilon){
                float invMassA = partA.invMass;
                float invMassB = partB.invMass;
                float diff = (deltaLength - restDistance) / (deltaLength * (invMassA + invMassB));
                posA.x += invMassA * dx * diff;
                posA.y += invMassA * dy * diff;
                partA.position = posA;

                posB.x -= invMassB * dx * diff;
                posB.y -= invMassB * dy * diff;
                partB.position = posB;
            }
       
        }
    }
}
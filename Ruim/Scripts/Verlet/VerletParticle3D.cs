﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Ruim.Verlet
{
    public class VerletParticle3D : MonoBehaviour
    {

        public Vector3 position
        {
            get
            {
                if (_trans == null)
                {
                    _trans = transform;
                }
                return _trans.position;
            }
            set
            {
                _trans.position = value;
            }
        }

        public float mass
        {
            get
            {
                return _mass;
            }
            set
            {
                _mass = value;
                if (_mass <= 0)
                {
                    _mass = 0;
                    _invMass = 0;
                }
                else
                {
                    _invMass = 1f / _mass;
                }
            }
        }

        public float invMass{
            get{
                return _invMass;
            }
        }

        public float drag{
            get{
                return _drag;
            }
            set{
                _drag = Mathf.Clamp01(value);
            }
        }

        public void AddForce(Vector3 f){
            _forces += f;
        }

        public void AddImpulse(Vector3 i){
            _prevPos -= i * _invMass;
        }

        public void StopMovement(){
            _forces.Set(0, 0, 0);
            _prevPos = position;
        }

        public Vector3 velocity{
            get{
                return position - _prevPos;
            }
            set{
                _prevPos = position - value;
            }
        }

        public void ClampSpeed(float min, float max){
            var v = velocity;
            var speedSq = v.sqrMagnitude;
            if(speedSq < min){
                velocity = v.normalized * min;
            } else if(speedSq > max){
                velocity = v.normalized * max;
            }
        }

        public Transform trans{
            get{
                return _trans;
            }
        }

        Vector3 _prevPos;
        Vector3 _forces;

        [SerializeField]
        float _mass = 1;
        float _invMass = 1;

        [SerializeField]
        [Range(0f, 1f)]
        float _drag = 0.95f;
        Transform _trans;

        // Use this for initialization
        void Awake()
        {
            if (_trans == null){
                _trans = transform;
            }
            var pos = _trans.position;
            _prevPos = pos;
            _forces = new Vector3();
        }

        // Update is called once per frame
        void Update()
        {

            float delta = Time.deltaTime;
            if(delta < Mathf.Epsilon){
                return; //is paused
            }

            var pos = _trans.position;
            var temp = pos;

            pos.x += (pos.x - _prevPos.x) * drag + _forces.x * _invMass * delta;
            pos.y += (pos.y - _prevPos.y) * drag + _forces.y * _invMass * delta;

            _prevPos = temp;
            _forces.Set(0, 0, 0);

            _trans.position = pos;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.UserInput
{
    public enum InputEventType
    {
        Down, Up, Drag
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.UserInput
{

    public struct InputEventData
    {

        public InputManager sender
        {
            get;
            private set;
        }

        public InputEventType eventType
        {
            get;
            private set;
        }

        public Vector3 position
        {
            get;
            private set;
        }

        public Vector3 delta
        {
            get;
            private set;
        }

        public int fingerID
        {
            get;
            private set;
        }

        public InputEventData(InputEventType eventType, InputManager sender, Vector3 position, int fingerID, Vector3 delta = new Vector3())
        {
            this.eventType = eventType;
            this.sender = sender;
            this.position = position;
            this.fingerID = fingerID;
            this.delta = delta;
        }
    }
}
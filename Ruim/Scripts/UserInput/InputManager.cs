﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Ruim.UserInput
{
    public class InputManager : MonoBehaviour
    {

        public delegate void InputEventDelegate(InputEventData data);

        public InputEventDelegate onEvent;

        public bool isPointerOverUI
        {
            get
            {
#if UNITY_IOS || UNITY_ANDROID
#if UNITY_EDITOR
                return EventSystem.current.IsPointerOverGameObject();
#else
                var numTouches = Input.touchCount;
                for (int i = 0; i < numTouches; i++)
                {
                    var t = Input.GetTouch(i);
                    if (EventSystem.current.IsPointerOverGameObject(t.fingerId))
                    {
                        return true;
                    }
                }
                return false;
#endif
#else
    return EventSystem.current.IsPointerOverGameObject();
#endif
            }
        }

        Vector3 _prevPos;

        // Use this for initialization
        void Awake()
        {
            UnityEngine.Input.multiTouchEnabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                _prevPos = UnityEngine.Input.mousePosition;
                if (onEvent != null)
                {
                    var data = new InputEventData(InputEventType.Down, this, UnityEngine.Input.mousePosition, 0);
                    onEvent.Invoke(data);
                }
            }
            else if (UnityEngine.Input.GetMouseButton(0))
            {
                var pos = UnityEngine.Input.mousePosition;
                var delta = pos - _prevPos;
                if (delta.sqrMagnitude > Mathf.Epsilon)
                {
                    if (onEvent != null)
                    {
                        var data = new InputEventData(InputEventType.Drag, this, pos, 0, delta);
                        onEvent.Invoke(data);
                    }
                    _prevPos = pos;
                }
            }
            else if (UnityEngine.Input.GetMouseButtonUp(0) && onEvent != null)
            {
                var data = new InputEventData(InputEventType.Up, this, UnityEngine.Input.mousePosition, 0);
                onEvent.Invoke(data);
            }
        }
    }
}
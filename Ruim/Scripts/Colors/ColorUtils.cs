using System.Collections.Generic;
using Ruim.Math;
using UnityEngine;


namespace Ruim.Colors
{
    public static class ColorUtils
    {
        public static Color ChangeBrightness(Color color, float brighChange)
        {
            float h, s, b;
            Color.RGBToHSV(color, out h, out s, out b);
            b = Mathf.Clamp01(b + brighChange);
            return Color.HSVToRGB(h, s, b);
        }

        public static void ChangeBrightness(ref Color color, float brightChange)
        {
            color = ChangeBrightness(color, brightChange);
        }

        public static Color MaxBrightness(Color color)
        {
            float h, s, b;
            Color.RGBToHSV(color, out h, out s, out b);
            return Color.HSVToRGB(h, s, 1f);
        }

        public static void MaxBrightness(ref Color color)
        {
            color = MaxBrightness(color);
        }

        public static Color ChangeSaturation(Color color, float satChange)
        {
            float h, s, b;
            Color.RGBToHSV(color, out h, out s, out b);
            s = Mathf.Clamp01(s + satChange);
            return Color.HSVToRGB(h, s, b);
        }

        public static void ChangeSaturation(ref Color color, float satChange)
        {
            color = ChangeSaturation(color, satChange);
        }

        public static Color MaxSaturation(Color color)
        {
            float h, s, b;
            Color.RGBToHSV(color, out h, out s, out b);
            return Color.HSVToRGB(h, s, 1f);
        }

        public static void MaxSaturation(ref Color color)
        {
            color = MaxSaturation(color);
        }

        public static Color ChangeHue(Color color, float hueChange)
        {
            float h, s, b;
            Color.RGBToHSV(color, out h, out s, out b);
            h = Mathf.Repeat(h + hueChange, 1f);
            return Color.HSVToRGB(h, s, b);
        }

        public static Color ChangeHue(Color color, Angle hueChange)
        {
            return ChangeHue(color, hueChange.degrees * INV_360);
        }

        public static void ChangeHue(ref Color color, float hueChange)
        {
            color = ChangeHue(color, hueChange);
        }

        public static void ChangeHue(ref Color color, Angle hueChange)
        {
            color = ChangeHue(color, hueChange);
        }

        public static float GetHue(Color color)
        {
            float h, s, b;
            Color.RGBToHSV(color, out h, out s, out b);
            return h;
        }

        static float INV_360 = 1f / 360f;
    }
}
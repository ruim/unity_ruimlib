﻿using System.Collections.Generic;
using Ruim.Math;
using UnityEngine;


namespace Ruim.Colors.Harmonies
{
    public abstract class BaseHarmony : MonoBehaviour
    {

        [SerializeField] Color _baseColor;
        [SerializeField] bool _createOnAwake;

        public abstract HarmonyType harmonyType { get; }

        [SerializeField] protected Color[] _palette = new Color[5];

        public Color[] palette => _palette;
        public Color baseColor => _baseColor;

        public void Create(Color baseColor)
        {
            _baseColor = baseColor;
            CreatePalette(baseColor);
        }

        protected abstract void CreatePalette(Color baseColor);

        protected static float INV_360 = 1f / 360f;

        protected virtual void Awake()
        {
            //     _palette = new Color[5];
            if (_createOnAwake)
            {
                CreatePalette(_baseColor);
            }
        }

        protected Color GetShifted(Color color, Angle hueChange, float satChange, float brightChange)
        {
            float h, s, b;
            Color.RGBToHSV(color, out h, out s, out b);
            var hueNorm = hueChange.degrees * INV_360;
            h = Mathf.Repeat(h + hueNorm, 1f);
            s = Mathf.Clamp01(s + satChange);
            b = Mathf.Clamp01(b + brightChange);
            return Color.HSVToRGB(h, s, b);
        }


    }
}
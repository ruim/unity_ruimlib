﻿namespace Ruim.Colors.Harmonies
{
    public enum HarmonyType
    {
        Analogous, Monochromatic, Triad, Complementary, Compound, Shades
    }
}
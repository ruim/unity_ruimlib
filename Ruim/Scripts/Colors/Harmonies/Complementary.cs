using System.Collections.Generic;
using UnityEngine;

using Ruim.Math;

namespace Ruim.Colors.Harmonies
{
    public class Complementary : BaseHarmony
    {
        public override HarmonyType harmonyType => HarmonyType.Complementary;

        protected override void CreatePalette(Color baseColor)
        {
            _palette[0] = GetShifted(baseColor, Angle.zero, 0.2f, -0.3f);
            _palette[1] = GetShifted(baseColor, Angle.zero, -0.1f, 0.3f);
            _palette[2] = baseColor;
            _palette[3] = GetShifted(baseColor, Angle.deg180, 0.2f, -0.3f);
            _palette[4] = GetShifted(baseColor, Angle.deg180, 0f, 0f);
        }
    }
}
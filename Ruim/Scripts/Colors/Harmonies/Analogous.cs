using System.Collections.Generic;
using UnityEngine;

using Ruim.Math;

namespace Ruim.Colors.Harmonies
{
    public class Analogous : BaseHarmony
    {
        public override HarmonyType harmonyType => HarmonyType.Analogous;

        protected override void CreatePalette(Color baseColor)
        {
            _palette[0] = GetShifted(baseColor, Angle.Degrees(-30), 0.05f, 0.05f);
            _palette[1] = GetShifted(baseColor, Angle.Degrees(-15f), 0.05f, 0.09f);
            _palette[2] = baseColor;
            _palette[3] = GetShifted(baseColor, Angle.Degrees(15f), 0.05f, 0.09f);
            _palette[4] = GetShifted(baseColor, Angle.Degrees(30f), 0.05f, 0.05f);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Cam
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class CameraBackgroundQuad : MonoBehaviour
    {

        public Vector3 scale { get; private set; }

        public void Init(Camera cam)
        {
            var transCache = transform;
            var distance = Mathf.Abs(transCache.localPosition.z);
            float h = Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad * 0.5f) * distance * 2f;

            scale = new Vector3(h * cam.aspect, h, 1f);
            transCache.localScale = scale;
        }

        public void SetColors(Color top, Color bottom)
        {
            _colors[2] = _colors[3] = top;
            _colors[0] = _colors[1] = bottom;
            _mesh.colors32 = _colors;
        }

        Color32[] _colors;
        Mesh _mesh;



        // Use this for initialization
        void Awake()
        {
            _mesh = GetComponent<MeshFilter>().mesh;
            _colors = new Color32[_mesh.vertexCount];
        }


    }
}
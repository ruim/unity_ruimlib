﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Cam
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField] Transform _cameraTransform;

        public void Apply(float amount, float dur)
        {
            _shakeAmount = Mathf.Max(_shakeAmount, amount);
            _duration = Mathf.Max(_duration, dur);
        }

        float _shakeAmount;
        float _duration;

        // Update is called once per frame
        void Update()
        {
            if (_duration > 0)
            {
                _duration -= Time.deltaTime;
                if (_duration <= 0)
                {
                    _cameraTransform.localPosition = new Vector3(0, 0, 0);
                    _duration = 0;
                }
                else
                {
                    float x = Random.Range(-_shakeAmount, _shakeAmount) * 0.5f;
                    float y = Random.Range(-_shakeAmount, _shakeAmount) * 0.5f;
                    _cameraTransform.localPosition = new Vector3(x, y, 0);
                }
            }
        }
    }
}
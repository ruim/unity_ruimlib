﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Cam
{
    [RequireComponent(typeof(Camera))]
    public class CameraBounds2D : MonoBehaviour
    {

        public Vector2 min{
            get{
                var pos = (Vector2)_trans.position;
                pos.x -= _width * 0.5f;
                pos.y -= _height * 0.5f;
                return pos;
            }
        }

        public Vector2 max{
            get{
                var pos = (Vector2)_trans.position;
                pos.x += _width * 0.5f;
                pos.y += _height * 0.5f;
                return pos;
            }
        }

        public float height{
            get{
                return _height;
            }
        }

        public float width{
            get{
                return _width;
            }
        }

        Transform _trans;
        Camera _cam;
        float _aspect;
        float _height;
        float _width;

        // Use this for initialization
        void Awake()
        {
            _cam = GetComponent<Camera>();
            _aspect = (float)Screen.width / (float)Screen.height;
            _height = _cam.orthographicSize * 2f;
            _width = _height * _aspect;
            _trans = transform;
        }

		private void OnDrawGizmos()
		{
            if(_trans != null){
                Gizmos.color = Color.green;
                var size = new Vector3(_width, _height, 1);
                Gizmos.DrawWireCube(_trans.position, size);
            }
		}

	}
}
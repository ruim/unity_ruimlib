﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Cam
{
    public class CamScaleUtils2D : MonoBehaviour
    {

        public Camera cam;


        public enum ScaleType{
            FixedWidth, FixedHeight
        }

        public ScaleType scaleType = ScaleType.FixedWidth;

        public enum ApplyType{
            OnAwake, OnStart, Manual
        }

        public ApplyType applyType;

    
        public float cameraWidth = 4.5f, cameraHeight = 8;

        public float aspect{
            get;
            private set;
        }

        public bool reset;

        void Awake(){
            
            if(applyType == ApplyType.OnAwake){
                Apply();
            }
        }


        void Start(){
            cam = GetComponent<Camera>();
            if (applyType == ApplyType.OnStart)
            {
                Apply();
            }
            
        }

        void Update(){
            if(reset){
                reset = false;
                Apply();
            }
        }


        public void Apply(){
            aspect = (float)Screen.width / (float)Screen.height;
            switch (scaleType)
            {
                case ScaleType.FixedWidth:
                    cameraHeight = cameraWidth / aspect;
                    cam.orthographicSize = cameraHeight * 0.5f;
                    break;
                case ScaleType.FixedHeight:
                    cam.orthographicSize = cameraHeight * 0.5f;
                    cameraWidth = cameraHeight * aspect;
                    break;
            }
        }

    }
}
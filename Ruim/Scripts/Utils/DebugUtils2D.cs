﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Utils
{
    public class DebugUtils2D
    {

        public static void GizmosDrawCircle(Vector3 center, float radius, int segments, Color color){
            float angle = 0;
            float angleAdd = (Mathf.PI * 2f) / (float)(segments - 1);
            float z = center.x;
            Gizmos.color = color;
            for (int i = 0; i < segments-1; i++){
                float x = center.x + Mathf.Cos(angle) * radius;
                float y = center.y + Mathf.Sin(angle) * radius;

                var p1 = new Vector3(x, y, z);

                angle += angleAdd;
                x = center.x + Mathf.Cos(angle) * radius;
                y = center.y + Mathf.Sin(angle) * radius;
                var p2 = new Vector3(x, y, z);

                Gizmos.DrawLine(p1, p2);
            }
        }
    }
}
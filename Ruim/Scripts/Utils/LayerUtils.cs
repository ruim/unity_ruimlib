﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Utils
{
    public class LayerUtils
    {
        public static bool LayerInMask(int layer, LayerMask mask)
        {
            return mask == (mask | (1 << layer));
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Utils
{
    public class BehaviourPool<T> : MonoBehaviour where T : MonoBehaviour
    {

        public int initialSize;
        public bool instantiateIfNeeded = true;
        public bool setPoolAsParent = true;

        public T prefab;

        public void AddToPool(T obj)
        {
            if (setPoolAsParent)
            {
                obj.transform.parent = _trans;
            }

            obj.gameObject.SetActive(false);

            _pool.Add(obj);
        }

        public T GetNext()
        {
            int num = _pool.Count;
            T obj = null;
            if (num == 0)
            {
                if (instantiateIfNeeded)
                {
                    obj = Instantiate<T>(prefab);
                    if (setPoolAsParent)
                    {
                        obj.transform.parent = _trans;
                    }
                }
            }
            else
            {
                obj = _pool[num - 1];
                obj.gameObject.SetActive(true);
                _pool.RemoveAt(num - 1);
            }

            return obj;
        }

        List<T> _pool;
        Transform _trans;

        protected virtual void Awake()
        {
            _trans = transform;
            _pool = new List<T>(initialSize);
            for (int i = 0; i < initialSize; i++)
            {
                var obj = Instantiate<T>(prefab);
                if (setPoolAsParent)
                {
                    obj.transform.parent = _trans;
                }
                obj.gameObject.SetActive(false);
                _pool.Add(obj);
            }
        }
    }
}
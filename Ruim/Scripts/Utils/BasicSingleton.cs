﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Utils
{
    public class BasicSingleton<T> : MonoBehaviour where T:BasicSingleton<T>
    {
        public static T instance{
            get;
            private set;
        }

        protected virtual void Awake(){
            if(instance != null){
                Destroy(this);
            }
            instance = (T)this;
        }
       
    }
}
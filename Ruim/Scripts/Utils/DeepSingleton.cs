﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Utils
{
    public class DeepSingleton<T> : MonoBehaviour where T : DeepSingleton<T>
    {

        public static T instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = new GameObject();
                    _instance = go.AddComponent<T>();
                    DontDestroyOnLoad(go);
                }
                return _instance;
            }
        }

        static T _instance;

        protected virtual void Awake()
        {
            if (_instance != null)
            {
                Destroy(this);
            }
            _instance = (T)this;
        }
    }
}
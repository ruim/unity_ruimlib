﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Ruim.Utils
{
    public class ScreenshotCapture : MonoBehaviour
    {


        public KeyCode saveKey;
        public string folder;
        public string prefix;

        int _counter;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(saveKey))
            {
                if (Directory.Exists(folder) == false)
                {
                    Debug.Log("does not exist");
                    Directory.CreateDirectory(folder);
                }
                var file = folder + "/" + prefix + _counter.ToString() + ".png";
                ScreenCapture.CaptureScreenshot(file);
                Debug.Log("saved - " + file);
                _counter++;
            }
        }

    }
}
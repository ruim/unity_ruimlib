﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Math
{
    public class ProbabilitySpace<T> : MonoBehaviour
    {
        public void Add(T item, Probability probability)
        {
            _items.Add(item);
            _probabilities.Add(probability);
            _needsNormalizing = true;
        }

        public void NormalizeProbabilitySpace()
        {
            var num = numItems;
            if (num == 0)
            {
                return;
            }
            var total = 0f;
            for (int i = 0; i < num; i++)
            {
                total += _probabilities[i].normalized;
            }
            if (total > 0f)
            {
                for (int i = 0; i < num; i++)
                {
                    _probabilities[i] /= total;
                }
            }
            _needsNormalizing = false;
        }

        public void ChangeItemProbability(T item, Probability change)
        {
            if (_needsNormalizing)
            {
                NormalizeProbabilitySpace();
            }
            var index = _items.IndexOf(item);
            _probabilities[index] += change;

            var count = _items.Count;
            var probSub = change * (float)(count - 1);
            for (int i = 0; i < count; i++)
            {
                if (i == index)
                {
                    continue;
                }
                _probabilities[i] -= probSub;
            }
        }

        public int numItems => _items.Count;

        public void Clear()
        {
            _items.Clear();
            _probabilities.Clear();
        }

        public T randomItem
        {
            get
            {
                if (_needsNormalizing)
                {
                    NormalizeProbabilitySpace();
                }
                var prob = Random.value;
                var accum = 0f;
                var num = numItems;
                for (int i = 0; i < num; i++)
                {
                    accum += _probabilities[i].normalized;

                    if (accum >= prob)
                    {
                        return _items[i];
                    }
                }

                return default(T);
            }
        }

        protected virtual void Awake()
        {
            _items = new List<T>();
            _probabilities = new List<Probability>();
        }

        List<T> _items;
        List<Probability> _probabilities;

        bool _needsNormalizing;
    }
}
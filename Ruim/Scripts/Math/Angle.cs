using UnityEngine;
using System.Collections;

namespace Ruim.Math
{
    [System.Serializable]
    public struct Angle
    {

        public static Angle VectorAngle(Vector2 v)
        {
            float deg = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
            return new Angle(deg);
        }

        public static Angle Degrees(float deg)
        {
            return new Angle(deg);
        }

        public static Angle Radians(float rads)
        {
            return new Angle(rads * Mathf.Rad2Deg);
        }

        public static Angle Add(Angle a1, Angle a2)
        {
            return new Angle(a1.degrees + a2.degrees);
        }

        public static Angle Sub(Angle a1, Angle a2)
        {
            return new Angle(a1.degrees - a2.degrees);
        }

        public static Angle Atan2(float y, float x)
        {
            var degs = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
            return new Angle(degs);
        }

        public static Angle operator +(Angle a, Angle b)
        {
            return new Angle(a.degrees + b.degrees);
        }

        public static Angle operator -(Angle a, Angle b)
        {
            return new Angle(a.degrees - b.degrees);
        }

        public static Angle operator *(Angle angle, float val)
        {
            return new Angle(angle.degrees * val);
        }

        public static Angle operator /(Angle angle, float val)
        {
            return new Angle(angle.degrees / val);
        }

        public static Angle operator -(Angle angle)
        {
            return angle * -1f;
        }

        public static Angle Lerp(Angle a1, Angle a2, float lerp)
        {
            float res = Mathf.LerpAngle(a1.degrees, a2.degrees, lerp);
            return Angle.Degrees(res);
        }

        public static Angle Diff(Angle a1, Angle a2)
        {
            float diffDegress = Mathf.DeltaAngle(a1.degrees, a2.degrees);
            return Angle.Degrees(diffDegress);
        }

        public static bool Match(Angle a1, Angle a2, Angle tollerance)
        {
            float diffDeg = Mathf.DeltaAngle(a1.degrees, a2.degrees);
            return diffDeg <= tollerance.degrees;
        }

        public static Angle zero
        {
            get
            {
                return new Angle(0);
            }
        }

        public static Angle fullCircle
        {
            get
            {
                return twoPi;
            }
        }

        public static Angle twoPi
        {
            get
            {
                return new Angle(360);
            }
        }

        public static Angle pi
        {
            get
            {
                return new Angle(180);
            }
        }

        public static Angle halfPi
        {
            get
            {
                return new Angle(90);
            }
        }

        public static Angle randomCircle
        {
            get
            {
                return new Angle(Random.Range(0, 360));
            }
        }

        public static Angle deg360
        {
            get
            {
                return new Angle(360);
            }
        }

        public static Angle deg270
        {
            get
            {
                return new Angle(270);
            }
        }

        public static Angle deg180
        {
            get
            {
                return new Angle(180);
            }
        }

        public static Angle deg90
        {
            get
            {
                return new Angle(90);
            }
        }

        public static Angle deg45
        {
            get
            {
                return new Angle(45);
            }
        }

        public float degrees
        {
            get
            {
                return _degrees;
            }

        }

        public float radians
        {
            get
            {
                return _degrees * Mathf.Deg2Rad;
            }

        }

        public float sine
        {
            get
            {
                return Mathf.Sin(_degrees * Mathf.Deg2Rad);
            }
        }

        public float cosine
        {
            get
            {
                return Mathf.Cos(_degrees * Mathf.Deg2Rad);
            }
        }

        public Vector2 directionVec
        {
            get
            {
                var rads = _degrees * Mathf.Deg2Rad;
                float x = Mathf.Cos(rads);
                float y = Mathf.Sin(rads);
                return new Vector2(x, y);
            }
        }

        public void Add(Angle other)
        {
            _degrees += other.degrees;
        }

        public void Sub(Angle other)
        {
            _degrees -= other.degrees;
        }

        public void Scale(float amount)
        {
            _degrees *= amount;
        }

        public void Div(float amount)
        {
            _degrees /= amount;
        }

        public bool Match(Angle other, Angle tollerance)
        {
            float diffDeg = Mathf.DeltaAngle(_degrees, other.degrees);
            return diffDeg <= tollerance.degrees;
        }

        public void Lerp(Angle other, float lerp)
        {
            _degrees = Mathf.LerpAngle(_degrees, other.degrees, lerp);
        }

        public override string ToString()
        {
            return "Angle degrees: " + _degrees;
        }

        private Angle(float degrees)
        {
            _degrees = degrees;
        }

        [SerializeField]
        float _degrees;
    }
}
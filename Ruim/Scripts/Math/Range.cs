﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Math
{
    [System.Serializable]
    public struct RangeI
    {

        public RangeI(int min, int max)
        {
            this.min = min;
            this.max = max;
        }

        public int min;
        public int max;

        public int diff
        {
            get
            {
                return max - min;
            }
        }

        public int random
        {
            get
            {
                return Random.Range(min, max);
            }
        }

        public void Set(int min, int max)
        {
            this.min = min;
            this.max = max;
        }

    }

    [System.Serializable]
    public struct RangeF
    {

        public RangeF(float min, float max)
        {
            this.min = min;
            this.max = max;
        }

        public float min;
        public float max;

        public float diff
        {
            get
            {
                return max - min;
            }
        }

        public float random
        {
            get
            {
                return Random.Range(min, max);
            }
        }

        public float Lerp(float amount)
        {
            return Mathf.Lerp(min, max, amount);
        }

        public void Set(float min, float max)
        {
            this.min = min;
            this.max = max;
        }
    }

    [System.Serializable]
    public struct RangeVector2
    {

        public RangeVector2(Vector2 min, Vector2 max)
        {
            this.min = min;
            this.max = max;
        }

        public Vector2 min;
        public Vector2 max;

        public Vector2 diff
        {
            get
            {
                return max - min;
            }
        }

        public Vector2 random
        {
            get
            {
                float x = Random.Range(min.x, max.x);
                float y = Random.Range(min.y, max.y);
                return new Vector2(x, y);
            }
        }

        public Vector2 Lerp(float amount)
        {
            return Vector2.Lerp(min, max, amount);
        }

        public void Set(Vector2 min, Vector2 max)
        {
            this.min = min;
            this.max = max;
        }
    }

    [System.Serializable]
    public struct RangeVector3
    {

        public RangeVector3(Vector3 min, Vector3 max)
        {
            this.min = min;
            this.max = max;
        }

        public Vector3 min;
        public Vector3 max;

        public Vector3 diff
        {
            get
            {
                return max - min;
            }
        }

        public Vector3 random
        {
            get
            {
                float x = Random.Range(min.x, max.x);
                float y = Random.Range(min.y, max.y);
                float z = Random.Range(min.z, max.z);
                return new Vector3(x, y, z);
            }
        }

        public Vector3 Lerp(float amount)
        {
            return Vector3.Lerp(min, max, amount);
        }

        public void Set(Vector3 min, Vector3 max)
        {
            this.min = min;
            this.max = max;
        }
    }

    [System.Serializable]
    public struct RangeColor
    {
        public RangeColor(Color min, Color max)
        {
            this.min = min;
            this.max = max;
        }

        public Color diff
        {
            get
            {
                var res = new Color(max.r - min.r, max.g - min.g, max.b - min.b, max.a - min.a);
                return res;
            }
        }

        public Color random
        {
            get
            {
                return Lerp(Random.value);
            }
        }

        public Color Lerp(float amount)
        {
            var r = Mathf.Lerp(min.r, max.r, amount);
            var g = Mathf.Lerp(min.g, max.g, amount);
            var b = Mathf.Lerp(min.b, max.b, amount);
            var a = Mathf.Lerp(max.a, min.a, amount);
            return new Color(r, g, b, a);
        }

        public void Set(Color min, Color max)
        {
            this.min = min;
            this.max = max;
        }

        public Color min, max;
    }

    [System.Serializable]
    public struct RangeAngle
    {

        public RangeAngle(Angle min, Angle max)
        {
            this.min = min;
            this.max = max;
        }

        public Angle shortestDiff
        {
            get
            {
                var degs = Mathf.DeltaAngle(min.degrees, max.degrees);
                return Angle.Degrees(degs);
            }
        }

        public Angle diff
        {
            get
            {
                return max - min;
            }
        }

        public Angle random
        {
            get
            {
                return Lerp(Random.value);
            }
        }

        public Angle Lerp(float amount)
        {
            var degs = Mathf.LerpAngle(min.degrees, max.degrees, amount);
            return Angle.Degrees(degs);
        }

        public void Set(Angle min, Angle max)
        {
            this.min = min;
            this.max = max;
        }

        public Angle min;
        public Angle max;

    }
}
﻿using System.Collections.Generic;
using UnityEngine;

//taken from here:
//https://easings.net/en#

namespace Ruim.Math
{
    public static class EasingExtensions
    {
        public static float Apply(this Easing ease, float t)
        {
            t = Mathf.Clamp01(t);
            switch (ease)
            {
                case Easing.InQuad: return EasingFunctions.InQuad(t);
                case Easing.OutQuad: return EasingFunctions.OutQuad(t);
                case Easing.InOutQuad: return EasingFunctions.InOutQuad(t);
                case Easing.InCubic: return EasingFunctions.InCubic(t);
                case Easing.OutCubic: return EasingFunctions.OutCubic(t);
                case Easing.InOutCubic: return EasingFunctions.InOutCubic(t);
                case Easing.InQuart: return EasingFunctions.InQuart(t);
                case Easing.OutQuart: return EasingFunctions.OutQuart(t);
                case Easing.InOutQuart: return EasingFunctions.InOutQuart(t);
                case Easing.InQuint: return EasingFunctions.InQuint(t);
                case Easing.OutQuint: return EasingFunctions.OutQuint(t);
                case Easing.InOutQuint: return EasingFunctions.InOutQuint(t);
                case Easing.InSine: return EasingFunctions.InSine(t);
                case Easing.OutSine: return EasingFunctions.OutSine(t);
                case Easing.InOutSine: return EasingFunctions.InOutSine(t);
                case Easing.InExpo: return EasingFunctions.InExpo(t);
                case Easing.OutExpo: return EasingFunctions.OutExpo(t);
                case Easing.InOutExpo: return EasingFunctions.InOutExpo(t);
                case Easing.InCirc: return EasingFunctions.InCirc(t);
                case Easing.OutCirc: return EasingFunctions.OutCirc(t);
                case Easing.InOutCirc: return EasingFunctions.InOutCirc(t);
                case Easing.InBack: return EasingFunctions.InBack(t);
                case Easing.OutBack: return EasingFunctions.OutBack(t);
                case Easing.InOutBack: return EasingFunctions.InOutBack(t);
                case Easing.InElastic: return EasingFunctions.InElastic(t);
                case Easing.OutElastic: return EasingFunctions.OutElastic(t);
                case Easing.InOutElastic: return EasingFunctions.InOutElastic(t);
                case Easing.InBounce: return EasingFunctions.InBounce(t);
                case Easing.OutBounce: return EasingFunctions.OutBounce(t);
                case Easing.InOutBounce: return EasingFunctions.InOutBounce(t);
                default: return t;
            }
        }

        public static float Apply(this Easing ease, float value, float target, float t)
        {
            return value + (target - value) * Apply(ease, t);
        }

        public static Vector3 Apply(this Easing ease, Vector3 value, Vector3 target, float t)
        {
            return value + (target - value) * Apply(ease, t);
        }

        public static Vector2 Apply(this Easing ease, Vector2 value, Vector2 target, float t)
        {
            return value + (target - value) * Apply(ease, t);
        }


    }
}
﻿using UnityEngine;
using System.Collections;

namespace Ruim.Math
{
    [System.Serializable]
    public struct Probability
    {

        public static Probability Normalized(float value)
        {
            return new Probability(value);
        }

        public static Probability Percent(float value)
        {
            return new Probability(value * 0.01f);
        }

        public static Probability Min(Probability a, Probability b)
        {
            var norm = Mathf.Min(a.normalized, b.normalized);
            return new Probability(norm);
        }

        public static Probability Max(Probability a, Probability b)
        {
            var norm = Mathf.Max(a.normalized, b.normalized);
            return new Probability(norm);
        }

        public static Probability operator +(Probability a, Probability b)
        {
            return Probability.Normalized(a.normalized + b.normalized);
        }

        public static Probability operator -(Probability a, Probability b)
        {
            return Probability.Normalized(a.normalized - b.normalized);
        }

        public static Probability operator *(Probability prob, float scale)
        {
            return Probability.Normalized(prob.normalized * scale);
        }

        public static Probability operator *(Probability a, Probability b)
        {
            return Probability.Normalized(a.normalized * b.normalized);
        }

        public static Probability operator /(Probability prob, float div)
        {
            return Probability.Normalized(prob.normalized / div);
        }

        public static Probability operator /(Probability a, Probability b)
        {
            return Probability.Normalized(a.normalized / b.normalized);
        }

        public static Probability zero => new Probability(0f);

        public static Probability fiftyFifty => new Probability(0.5f);

        public static Probability always => new Probability(1f);

        public float normalized
        {
            get
            {
                return _normalizedValue;
            }
        }

        public float percent
        {
            get
            {
                return _normalizedValue * 100f;
            }
        }
        /* 
                public void Add(Probability other)
                {
                    _normalizedValue += other.normalized;
                }

                public void Sub(Probability other)
                {
                    _normalizedValue -= other.normalized;
                }

                public void Scale(float scale)
                {
                    _normalizedValue *= scale;
                }

                public void Div(float div)
                {
                    _normalizedValue /= div;
                }*/

        public void Clamp(Probability max)
        {
            if (_normalizedValue > max.normalized)
            {
                _normalizedValue = max.normalized;
            }
        }

        public void Clamp(Probability min, Probability max)
        {
            if (_normalizedValue < min.normalized)
            {
                _normalizedValue = min.normalized;
            }
            if (_normalizedValue > max.normalized)
            {
                _normalizedValue = max.normalized;
            }
        }

        public bool Test()
        {
            var val = Random.value;
            return val < _normalizedValue;
        }

        private Probability(float normalizedValue)
        {
            _normalizedValue = normalizedValue;
        }

        [SerializeField]
        float _normalizedValue;

    }
}
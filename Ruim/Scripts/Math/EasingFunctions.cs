using System.Collections.Generic;
using UnityEngine;

//taken from here:
//https://easings.net/en#

namespace Ruim.Math
{
    public static class EasingFunctions
    {

        public static float Linear(float x)
        {
            return x;
        }

        public static float InQuad(float x)
        {
            return x * x;
        }
        public static float OutQuad(float x)
        {
            return 1f - (1f - x) * (1f - x);
        }
        public static float InOutQuad(float x)
        {
            return x < 0.5f ?
                2f * x * x :
                1f - Mathf.Pow(-2f * x + 2f, 2f) * 0.5f;
        }
        public static float InCubic(float x)
        {
            return x * x * x;
        }
        public static float OutCubic(float x)
        {
            return 1 - Mathf.Pow(1f - x, 3f);
        }
        public static float InOutCubic(float x)
        {
            return x < 0.5f ?
                4f * x * x * x :
                1f - Mathf.Pow(-2f * x + 2f, 3f) * 0.5f;
        }
        public static float InQuart(float x)
        {
            return x * x * x * x;
        }
        public static float OutQuart(float x)
        {
            return 1 - Mathf.Pow(1f - x, 4f);
        }
        public static float InOutQuart(float x)
        {
            return x < 0.5f ? (8f * x * x * x * x) : (1f - Mathf.Pow(-2f * x + 2f, 4f) * 0.5f);
        }
        public static float InQuint(float x)
        {
            return x * x * x * x * x;
        }
        public static float OutQuint(float x)
        {
            return 1f - Mathf.Pow(1f - x, 5f);
        }
        public static float InOutQuint(float x)
        {
            return x < 0.5f ? 16f * x * x * x * x * x : 1 - Mathf.Pow(-2f * x + 2f, 5f) * 0.5f;
        }
        public static float InSine(float x)
        {
            return 1f - Mathf.Cos(x * Mathf.PI * 0.5f);
        }
        public static float OutSine(float x)
        {
            return Mathf.Sin(x * Mathf.PI * 0.5f);
        }
        public static float InOutSine(float x)
        {
            return -(Mathf.Cos(Mathf.PI * x) - 1f) * 0.5f;
        }
        public static float InExpo(float x)
        {
            return x < Mathf.Epsilon ? 0f : Mathf.Pow(2f, 10f * x - 10f);
        }
        public static float OutExpo(float x)
        {
            return x > 1f - Mathf.Epsilon ? 1f : 1f - Mathf.Pow(2f, -10f * x);
        }
        public static float InOutExpo(float x)
        {
            return x < Mathf.Epsilon ? 0f : x > 1f - Mathf.Epsilon ? 1f : x < 0.5f ?
                Mathf.Pow(2f, 20f * x - 10f) * 0.5f :
                (2f - Mathf.Pow(2f, -20f * x + 10f)) * 0.5f;
        }
        public static float InCirc(float x)
        {
            return 1f - Mathf.Sqrt(1f - Mathf.Pow(x, 2f));
        }
        public static float OutCirc(float x)
        {
            return Mathf.Sqrt(1f - Mathf.Pow(x - 1f, 2f));
        }
        public static float InOutCirc(float x)
        {
            return x < 0.5f ?
                (1f - Mathf.Sqrt(1f - Mathf.Pow(2f * x, 2f))) * 0.5f :
                (Mathf.Sqrt(1f - Mathf.Pow(-2f * x + 2f, 2f)) + 1f) * 0.5f;
        }
        public static float InBack(float x)
        {
            return C3 * x * x * x - C1 * x * x;
        }
        public static float OutBack(float x)
        {
            return 1f + C3 * Mathf.Pow(x - 1f, 3f) + C1 * Mathf.Pow(x - 1f, 2f);
        }
        public static float InOutBack(float x)
        {
            return x < 0.5f ?
                (Mathf.Pow(2f * x, 2f) * ((C2 + 1f) * 2f * x - C2)) * 0.5f :
                (Mathf.Pow(2f * x - 2f, 2f) * ((C2 + 1f) * (x * 2f - 2f) + C2) + 2f) * 0.5f;
        }
        public static float InElastic(float x)
        {
            return x < Mathf.Epsilon ? 0f : x > 1f - Mathf.Epsilon ? 1f :
                -Mathf.Pow(2f, 10f * x - 10f) * Mathf.Sin((x * 10f - 10.75f) * C4);
        }
        public static float OutElastic(float x)
        {
            return x < Mathf.Epsilon ? 0f : x < 1f - Mathf.Epsilon ? 1f :
                Mathf.Pow(2, -10 * x) * Mathf.Sin((x * 10f - 0.75f) * C4) + 1f;
        }
        public static float InOutElastic(float x)
        {
            return x < Mathf.Epsilon ? 0f : x > 1f - Mathf.Epsilon ? 1f : x < 0.5f ?
                -(Mathf.Pow(2f, 20f * x - 10f) * Mathf.Sin((20f * x - 11.125f) * C5)) * 0.5f :
                Mathf.Pow(2f, -20f * x + 10f) * Mathf.Sin((20f * x - 11.125f) * C5) * 0.5f + 1;
        }
        public static float InBounce(float x)
        {
            return 1f - OutBounce(1f - x);
        }
        public static float OutBounce(float x)
        {
            var n1 = 7.5625f;
            var d1 = 2.75f;

            if (x < 1f / d1)
            {
                return n1 * x * x;
            }
            else if (x < 2f / d1)
            {
                return n1 * (x -= (1.5f / d1)) * x + 0.75f;
            }
            else if (x < 2.5f / d1)
            {
                return n1 * (x -= (2.25f / d1)) * x + 0.9375f;
            }
            else
            {
                return n1 * (x -= (2.625f / d1)) * x + 0.984375f;
            }
        }
        public static float InOutBounce(float x)
        {
            return x < 0.5f ?
                (1f - OutBounce(1f - 2f * x)) * 0.5f :
                (1f + OutBounce(2f * x - 1f)) * 0.5f;
        }

        static readonly float C1 = 1.70158f;
        static readonly float C2 = C1 * 1.525f;
        static readonly float C3 = C1 + 1f;
        static readonly float C4 = (2 * Mathf.PI) / 3f;
        static readonly float C5 = (2 * Mathf.PI) / 4.5f;
    }
}

/*
const pow = Math.pow;
const sqrt = Math.sqrt;
const sin = Math.sin;
const cos = Math.cos;
const PI = Math.PI;
const c1 = 1.70158;
const c2 = c1 * 1.525;
const c3 = c1 + 1;
const c4 = ( 2 * PI ) / 3;
const c5 = ( 2 * PI ) / 4.5;

function bounceOut(x: number) {
	const n1 = 7.5625;
	const d1 = 2.75;

	if ( x < 1 / d1 ) {
		return n1 * x * x;
	} else if ( x < 2 / d1 ) {
		return n1 * (x -= (1.5 / d1)) * x + .75;
	} else if ( x < 2.5 / d1 ) {
		return n1 * (x -= (2.25 / d1)) * x + .9375;
	} else {
		return n1 * (x -= (2.625 / d1)) * x + .984375;
	}
}

const easingsFunctions: any = {
	easeInQuad(x: number) {
		return x * x;
	},
	easeOutQuad(x: number) {
		return 1 - ( 1 - x ) * ( 1 - x );
	},
	easeInOutQuad(x: number) {
		return x < 0.5 ?
			2 * x * x :
			1 - pow( -2 * x + 2, 2 ) / 2;
	},
	easeInCubic(x: number) {
		return x * x * x;
	},
	easeOutCubic(x: number) {
		return 1 - pow( 1 - x, 3 );
	},
	easeInOutCubic(x: number) {
		return x < 0.5 ?
			4 * x * x * x :
			1 - pow( -2 * x + 2, 3 ) / 2;
	},
	easeInQuart(x: number) {
		return x * x * x * x;
	},
	easeOutQuart(x: number) {
		return 1 - pow( 1 - x, 4 );
	},
	easeInOutQuart(x: number) {
		return x < 0.5 ?
			8 * x * x * x * x :
			1 - pow( -2 * x + 2, 4 ) / 2;
	},
	easeInQuint(x: number) {
		return x * x * x * x * x;
	},
	easeOutQuint(x: number) {
		return 1 - pow( 1 - x, 5 );
	},
	easeInOutQuint(x: number) {
		return x < 0.5 ?
			16 * x * x * x * x * x :
			1 - pow( -2 * x + 2, 5 ) / 2;
	},
	easeInSine(x: number) {
		return 1 - cos( x * PI / 2 );
	},
	easeOutSine(x: number) {
		return sin( x * PI / 2 );
	},
	easeInOutSine(x: number) {
		return -( cos( PI * x ) - 1 ) / 2;
	},
	easeInExpo(x: number) {
		return x === 0 ? 0 : pow( 2, 10 * x - 10 );
	},
	easeOutExpo(x: number) {
		return x === 1 ? 1 : 1 - pow( 2, -10 * x );
	},
	easeInOutExpo(x: number) {
		return x === 0 ? 0 : x === 1 ? 1 : x < 0.5 ?
			pow( 2, 20 * x - 10 ) / 2 :
			( 2 - pow( 2, -20 * x + 10 ) ) / 2;
	},
	easeInCirc(x: number) {
		return 1 - sqrt( 1 - pow( x, 2 ) );
	},
	easeOutCirc(x: number) {
		return sqrt( 1 - pow( x - 1, 2 ) );
	},
	easeInOutCirc(x: number) {
		return x < 0.5 ?
			( 1 - sqrt( 1 - pow( 2 * x, 2 ) ) ) / 2 :
			( sqrt( 1 - pow( -2 * x + 2, 2 ) ) + 1 ) / 2;
	},
	easeInBack(x: number) {
		return c3 * x * x * x - c1 * x * x;
	},
	easeOutBack(x: number) {
		return 1 + c3 * pow( x - 1, 3 ) + c1 * pow( x - 1, 2 );
	},
	easeInOutBack(x: number) {
		return x < 0.5 ?
			( pow( 2 * x, 2 ) * ( ( c2 + 1 ) * 2 * x - c2 ) ) / 2 :
			( pow( 2 * x - 2, 2 ) * ( ( c2 + 1 ) * ( x * 2 - 2 ) + c2 ) + 2 ) / 2;
	},
	easeInElastic(x: number) {
		return x === 0 ? 0 : x === 1 ? 1 :
			-pow( 2, 10 * x - 10 ) * sin( ( x * 10 - 10.75 ) * c4 );
	},
	easeOutElastic(x: number) {
		return x === 0 ? 0 : x === 1 ? 1 :
			pow( 2, -10 * x ) * sin( ( x * 10 - 0.75 ) * c4 ) + 1;
	},
	easeInOutElastic(x: number) {
		return x === 0 ? 0 : x === 1 ? 1 : x < 0.5 ?
			-( pow( 2, 20 * x - 10 ) * sin( ( 20 * x - 11.125 ) * c5 )) / 2 :
			pow( 2, -20 * x + 10 ) * sin( ( 20 * x - 11.125 ) * c5 ) / 2 + 1;
	},
	easeInBounce(x: number) {
		return 1 - bounceOut( 1 - x );
	},
	easeOutBounce: bounceOut,
	easeInOutBounce(x: number) {
		return x < 0.5 ?
			( 1 - bounceOut( 1 - 2 * x ) ) / 2 :
			( 1 + bounceOut( 2 * x - 1 ) ) / 2;
	},
};

 */

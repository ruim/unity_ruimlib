﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Math
{
    public class LerpPos : MonoBehaviour
    {

        public bool useUnscaledTime = true;

        public void LerpTo(Vector3 target, float speed){
            _target = target;
            _speed = speed;
            _progress = 0;
        }

        public void LerpFrom(Vector3 origin, float speed)
        {
            var target = _trans.position;
            LerpFromTo(origin, target, speed);
        }

        public void LerpFromTo(Vector3 origin, Vector3 target, float speed){
            _trans.position = origin;
            LerpTo(target, speed);
        }

        public Transform trans{
            get{
                if(_trans == null){
                    _trans = transform;
                }
                return _trans;
            }
        }

        Transform _trans;
        Vector3 _target;
        float _progress;
        float _speed;


        // Use this for initialization
        void Awake()
        {
            if (_trans == null)
            {
                _trans = transform;
            }
            _progress = 1; //closed
        }

        // Update is called once per frame
        void Update()
        {
            if(_progress < 1){
                if (useUnscaledTime)
                {
                    _progress += Time.unscaledDeltaTime * _speed;
                }
                else
                {
                    _progress += Time.deltaTime * _speed;
                }
                if(_progress > 1){
                    _progress = 1;
                }
                var pos = _trans.position;
                pos = Vector3.Lerp(pos, _target, _progress);
                _trans.position = pos;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



namespace Ruim.UI
{
    public class UIProgressStars : MonoBehaviour
    {

        [SerializeField] UIRewardStar[] _stars;
        [SerializeField] Text _numStrokesLabel;

        public enum NumStars
        {
            Zero, One, Two, Three
        }

        public void Show(int numStrokes, Color color, NumStars numStars)
        {

            for (int i = 0; i < _stars.Length; i++)
            {
                _stars[i].Hide();
                _stars[i].color = color;
            }


            switch (numStars)
            {
                case NumStars.Zero:
                    _numStars = 0;
                    break;
                case NumStars.One:
                    _numStars = 1;
                    break;
                case NumStars.Two:
                    _numStars = 2;
                    break;
                case NumStars.Three:
                    _numStars = 3;
                    break;
            }

            if (_numStars > 0)
            {
                var delay = 0f;
                for (int i = 0; i < _numStars; i++)
                {
                    var star = _stars[i];
                    star.Show(delay);
                    delay += 0.1f;
                }
            }


            if (numStrokes > 0)
            {
                _numStrokesLabel.enabled = true;
                if (numStrokes == 1)
                {
                    _numStrokesLabel.text = "HOLE IN ONE";
                }
                else
                {
                    _numStrokesLabel.text = numStrokes.ToString() + " STROKES";
                }

            }
            else
            {
                _numStrokesLabel.enabled = false;
            }


        }

        int _numStars;

    }
}
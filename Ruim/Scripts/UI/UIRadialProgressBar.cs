﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ruim.UI
{
    public class UIRadialProgressBar : MonoBehaviour
    {
        [SerializeField] Image _barImage;
        [SerializeField] Image _backImage;

        public Color frontColor
        {
            set
            {
                _barImage.color = value;
            }
        }

        public float progress
        {
            set
            {
                _barImage.fillAmount = Mathf.Clamp01(value);
            }
            get
            {
                return _barImage.fillAmount;
            }
        }

        void Awake()
        {
            SetupImage(_barImage);
            SetupImage(_backImage);
        }

        void SetupImage(Image img)
        {
            img.fillMethod = Image.FillMethod.Radial360;
            img.fillClockwise = true;

        }

    }
}
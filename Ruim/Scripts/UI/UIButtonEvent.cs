﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace Ruim.UI
{
    public class UIButtonEvent : UnityEvent<UIButton>
    {

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Ruim.Math;

namespace Ruim.UI
{
    public class UIMovingWord : MonoBehaviour
    {

        [SerializeField] UIMovingLetter _letterPrefab;

        [SerializeField] bool _initWithDefaultText;
        [SerializeField] string _defaultText;

        public Angle maxLetterRotation = Angle.Degrees(10f);
        public float letterRotationFrequency = 1f;
        public Angle angleAddPerLetter = Angle.fullCircle;

        public Color color
        {
            set
            {
                var num = _letters.Count;
                for (int i = 0; i < num; i++)
                {
                    _letters[i].color = value;
                }
            }
        }

        public int numLetters => _letters.Count;



        public void Init(string word)
        {
            if (_letters.Count > 0)
            {
                for (int i = 0; i < _letters.Count; i++)
                {
                    Destroy(_letters[i].gameObject);
                }
                _letters.Clear();
            }

            var len = word.Length;
            var textWidth = 0f;
            for (int i = 0; i < len; i++)
            {
                var letterCharacter = word[i].ToString();
                var letter = Instantiate<UIMovingLetter>(_letterPrefab, _trans);
                letter.Init(letterCharacter);
                textWidth += letter.width;
                _letters.Add(letter);
            }

            var startX = -textWidth * 0.5f + _letters[0].width * 0.1f;
            UIMovingLetter prevLetter = null;
            for (int i = 0; i < len; i++)
            {
                var letter = _letters[i];
                if (prevLetter != null)
                {
                    startX += prevLetter.width * 0.5f;
                }
                startX += letter.width * 0.5f;
                var pos = letter.localPosition;
                pos.x = startX;
                letter.localPosition = pos;

                prevLetter = letter;
            }

        }

        public void SetLetterColor(int index, Color color)
        {
            _letters[index].color = color;
        }

        List<UIMovingLetter> _letters;
        Transform _trans;

        float _counter;


        // Use this for initialization
        void Awake()
        {
            _letters = new List<UIMovingLetter>();
            _trans = transform;

            if (_initWithDefaultText)
            {
                Init(_defaultText);
            }
        }

        // Update is called once per frame
        void Update()
        {
            _counter += Time.deltaTime * letterRotationFrequency * Mathf.PI;
            var currCounter = _counter;
            var num = _letters.Count;
            var counterOffset = angleAddPerLetter / (float)num;

            for (int i = 0; i < num; i++)
            {
                var letter = _letters[i];
                var angle = maxLetterRotation * Mathf.Sin(currCounter);
                letter.rotation = angle;
                currCounter += counterOffset.radians;
            }
        }
    }
}
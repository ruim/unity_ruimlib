﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.UI
{
    public abstract class UIMenu : MonoBehaviour
    {
        [Header("Optional Fade Rect")]
        [SerializeField] UIFadeRect _fadeRect;

        [Header("Open/Close delay")]
        [SerializeField] float _delayBetweenItems;
        [SerializeField] float _openDelay;
        [SerializeField] float _closeDelay;

        [Header("Items to animate")]
        [SerializeField]
        List<UIMoveAnimation> _items;

        public virtual void OpenImmediate()
        {
            var num = _items.Count;
            for (int i = 0; i < num; i++)
            {
                _items[i].OpenImmediate();
            }
            if (_fadeRect != null)
            {
                _fadeRect.FadeInImmediate();
            }
        }

        public virtual void Open()
        {
            var num = _items.Count;
            var delay = _openDelay;
            for (int i = 0; i < num; i++)
            {
                _items[i].Open(delay);
                delay += _delayBetweenItems;
            }
            if (_fadeRect != null)
            {
                _fadeRect.FadeIn();
            }
        }

        public virtual void CloseImmediate()
        {
            var num = _items.Count;
            for (int i = 0; i < num; i++)
            {
                _items[i].CloseImmediate();
            }
            if (_fadeRect != null)
            {
                _fadeRect.FadeOutImmediate();
            }
        }

        public virtual void Close()
        {

            var num = _items.Count;
            var delay = _closeDelay;
            for (int i = 0; i < num; i++)
            {
                _items[i].Close(delay);
                delay += _delayBetweenItems;
            }
            if (_fadeRect != null)
            {
                _fadeRect.FadeOut();
            }
        }

        public void AddAnimatedItem(UIMoveAnimation item)
        {
            _items.Add(item);
        }

        public void RemoveAnimatedItem(UIMoveAnimation item)
        {
            _items.Remove(item);
        }






    }
}
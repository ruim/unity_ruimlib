﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ruim.UI
{
    [RequireComponent(typeof(Image))]
    public class UIFadeRect : MonoBehaviour
    {

        [SerializeField]
        Color _visibleColor;

        [SerializeField]
        float _fadeDuration;

        public void FadeIn()
        {
            _targetColor = _visibleColor;
            _fadeProgress = 0f;
        }

        public void FadeInImmediate()
        {
            _targetColor = _visibleColor;
            _fadeProgress = 1f;
            _img.color = _targetColor;
        }

        public void FadeOut()
        {
            _targetColor = _visibleColor;
            _targetColor.a = 0f;
            _fadeProgress = 0f;
        }

        public void FadeOutImmediate()
        {
            _targetColor = _visibleColor;
            _targetColor.a = 0f;
            _fadeProgress = 1f;
            _img.color = _targetColor;
        }

        Image _img;
        Color _targetColor;
        float _fadeProgress;

        // Use this for initialization
        void Awake()
        {
            _img = GetComponent<Image>();
            _fadeProgress = 1f;
            _img.color = _visibleColor;
        }

        // Update is called once per frame
        void Update()
        {
            if (_fadeProgress < 1f)
            {
                _fadeProgress += Time.deltaTime / _fadeDuration;
                if (_fadeProgress > 1f)
                {
                    _fadeProgress = 1f;
                }
                _img.color = Color.Lerp(_img.color, _targetColor, _fadeProgress);
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.UI
{
    public class UIScoreLabel : UILabel
    {
        [SerializeField]
        float _scoreAdd;

        public int score
        {
            set
            {
                _targetScore = value;
            }
        }

        public void SetScoreImmediate(int score)
        {
            _currScore = score;
            ApplyScoreText(score);
        }

        int _targetScore;
        float _currScore;

        private void Update()
        {
            if (_targetScore > (int)_currScore)
            {
                var toAdd = _scoreAdd * Time.unscaledDeltaTime;
                if (toAdd < 1f)
                {
                    toAdd = 1f;
                }
                _currScore += toAdd;
                if ((int)_currScore > _targetScore)
                {
                    ApplyScoreText(_targetScore);
                }
                else
                {
                    ApplyScoreText((int)_currScore);
                }
            }
        }

        void ApplyScoreText(int score)
        {
            var scoreText = score.ToString();
            mainText.text = scoreText;
            if (textShadow != null)
            {
                textShadow.text = scoreText;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



namespace Ruim.UI
{

    [RequireComponent(typeof(Slider))]
    public class UIProgressBar : MonoBehaviour
    {

        [SerializeField] float _progressLerpSpeed;


        [SerializeField] Image _fillImage;
        [SerializeField] Image _backImage;


        public float progress
        {
            set
            {
                _targetProgress = value;
            }
        }

        public Color color
        {
            set
            {
                _fillImage.color = value;
            }
        }

        float _progress, _targetProgress;
        Slider _slider;

        void Awake()
        {
            _slider = GetComponent<Slider>();
        }


        // Update is called once per frame
        void Update()
        {
            _progress = Mathf.Lerp(_progress, _targetProgress, _progressLerpSpeed * Time.unscaledDeltaTime);
            _slider.value = _progress;
        }


    }
}
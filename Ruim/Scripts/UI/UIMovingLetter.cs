﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using Ruim.Math;

namespace Ruim.UI
{
    public class UIMovingLetter : UILabel
    {


        public Vector2 localPosition
        {
            set
            {
                _trans.localPosition = (Vector3)value;
            }
            get
            {
                return _trans.localPosition;
            }
        }

        public Angle rotation
        {
            set
            {
                _trans.localRotation = Quaternion.Euler(0, 0, value.degrees);
            }
        }

        public float width
        {
            get;
            private set;
        }

        public void Init(string character)
        {
            text = character;
            var textRectTrans = mainText.GetComponent<RectTransform>();
            width = LayoutUtility.GetPreferredWidth(textRectTrans);

            _trans = transform;
        }

        Transform _trans;

    }
}
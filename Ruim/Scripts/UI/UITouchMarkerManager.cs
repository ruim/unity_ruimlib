﻿using UnityEngine;
using System.Collections.Generic;
using Ruim.UserInput;

namespace Ruim.UI
{
    [RequireComponent(typeof(UITouchMarkerPool))]
    public class UITouchMarkerManager : MonoBehaviour
    {

        public void Init(InputManager inputManager)
        {
            inputManager.onEvent += OnAgentForceEvent;
        }

        UITouchMarkerPool _pool;
        UITouchMarker _touchMarker;

        // Use this for initialization
        void Awake()
        {
            _pool = GetComponent<UITouchMarkerPool>();
        }

        void OnAgentForceEvent(InputEventData data)
        {
            switch (data.eventType)
            {
                case InputEventType.Down:
                    RemoveMarker();
                    _touchMarker = _pool.GetNext();
                    _touchMarker.Place(data.position);
                    break;
                case InputEventType.Up:
                    RemoveMarker();
                    break;
            }
        }

        void RemoveMarker()
        {
            if (_touchMarker != null)
            {
                _pool.AddToPool(_touchMarker);
                _touchMarker = null;
            }
        }

    }
}
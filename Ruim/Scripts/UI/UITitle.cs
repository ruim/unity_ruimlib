﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace Ruim.UI
{
    public class UITitle : MonoBehaviour
    {

        [SerializeField] string _movingWordText;

        [SerializeField] UILabel _titleTopWord;

        [SerializeField] UIMovingWord _movingWord;

        public void Init(Color color)
        {
            _movingWord.Init(_movingWordText);
            ResetColor(color);
        }

        public void ResetColor(Color color)
        {
            _movingWord.color = color;
        }
    }
}
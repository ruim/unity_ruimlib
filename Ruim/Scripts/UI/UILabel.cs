﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ruim.UI
{
    public class UILabel : MonoBehaviour
    {
        public Color color
        {
            get
            {
                return _text.color;
            }
            set
            {
                _text.color = value;
            }
        }



        public string text
        {
            get
            {
                return _text.text;
            }
            set
            {
                _text.text = value;
                if (_shadow != null)
                {
                    _shadow.text = value;
                }
            }
        }

        public bool visible
        {
            set
            {
                _text.enabled = value;
                if (_shadow != null)
                {
                    _shadow.enabled = value;
                }
            }
        }

        [SerializeField]
        Text _text;

        [SerializeField]
        Text _shadow;

        protected Text mainText
        {
            get
            {
                return _text;
            }
        }

        protected Text textShadow
        {
            get
            {
                return _shadow;
            }
        }


    }
}
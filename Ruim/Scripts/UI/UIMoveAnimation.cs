﻿using System.Collections;
using System.Collections.Generic;
using Ruim.Math;
using UnityEngine;
using UnityEngine.Serialization;

namespace Ruim.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class UIMoveAnimation : MonoBehaviour
    {
        [FormerlySerializedAs("offset")]
        [SerializeField] float _offset = 1000f;

        [FormerlySerializedAs("duration")]
        [SerializeField] float _duration = 0.5f;

        [FormerlySerializedAs("directionType")]
        [SerializeField] UIMoveDirectionType _directionType = UIMoveDirectionType.Down;

        public bool isOpen { get; private set; }

        public void OpenImmediate()
        {
            if (isOpen)
            {
                return;
            }
            isOpen = true;
            _transCache.position = _openedPos;
            _progress = 1f;
            gameObject.SetActive(true);
            _closing = false;
        }

        public void Open(float delay = 0f)
        {
            if (isOpen)
            {
                return;
            }
            isOpen = true;
            gameObject.SetActive(true);
            _closing = false;
            BeginAnim(_closedPos, _openedPos, delay);
        }

        public void CloseImmediate()
        {
            if (isOpen == false)
            {
                return;
            }
            isOpen = false;
            _transCache.position = _closedPos;
            _progress = 1f;
            gameObject.SetActive(false);
            _closing = false;
        }

        public void Close(float delay = 0f)
        {
            if (isOpen == false)
            {
                return;
            }
            isOpen = false;
            BeginAnim(_openedPos, _closedPos, delay);
            _closing = true;
        }

        RectTransform _transCache;
        Vector3 _openedPos;
        Vector3 _targetPos;

        float _progress, _progressMult;

        bool _closing;

        float _delayCountdown;



        // Use this for initialization
        void Awake()
        {
            _transCache = GetComponent<RectTransform>();
            _openedPos = _transCache.position;
            _progress = 1f;
            isOpen = true;
        }


        // Update is called once per frame
        void Update()
        {
            if (_progress < 1f)
            {
                var delta = Time.unscaledDeltaTime;
                if (_delayCountdown > 0f)
                {
                    _delayCountdown -= delta;
                    return;
                }
                _progress += delta * _progressMult;
                if (_progress > 1f)
                {
                    _progress = 1f;
                    if (_closing)
                    {
                        _closing = false;
                        gameObject.SetActive(false);
                    }
                }
                _transCache.position = Vector3.Lerp(_transCache.position, _targetPos, _progress);
            }
        }

        void BeginAnim(Vector3 startPos, Vector3 endPos, float delay)
        {
            _transCache.position = startPos;
            _delayCountdown = delay;
            _progress = 0f;
            _progressMult = 1f / _duration;
            _targetPos = endPos;
        }

        Vector3 _closedPos
        {
            get
            {
                var positionOffset = new Vector3();
                switch (_directionType)
                {
                    case UIMoveDirectionType.Down:
                        positionOffset.y = -_offset;
                        break;
                    case UIMoveDirectionType.Up:
                        positionOffset.y = _offset;
                        break;
                    case UIMoveDirectionType.Left:
                        positionOffset.x = -_offset;
                        break;
                    case UIMoveDirectionType.Right:
                        positionOffset.x = _offset;
                        break;
                }
                return _openedPos + positionOffset;
            }
        }
    }
}
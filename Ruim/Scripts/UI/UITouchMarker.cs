﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Ruim.Utils;

namespace Ruim.UI
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(Image))]
    public class UITouchMarker : MonoBehaviour
    {
        public void Place(Vector3 screenPos)
        {
            transform.position = screenPos;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Ruim.UI
{
    [RequireComponent(typeof(Button))]
    public class UIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] Image _image;
        [SerializeField] Vector3 _pressOffset = new Vector3(0f, -15f, 0f);

        public UIButtonEvent onClick
        {
            get;
            private set;
        }


        public bool interactable
        {
            get
            {
                return _button.interactable;
            }
            set
            {
                _button.interactable = value;
            }
        }

        public Color color
        {
            get
            {
                return _image.color;
            }
            set
            {
                _image.color = value;
            }
        }


        UnityEngine.UI.Button _button;
        RectTransform _imageTrans;
        Vector3 _defaultImagePos;

        void Awake()
        {
            _button = GetComponent<UnityEngine.UI.Button>();
            _button.onClick.AddListener(OnButtonClick);

            onClick = new UIButtonEvent();

            if (_image != null)
            {
                _imageTrans = _image.GetComponent<RectTransform>();
                _defaultImagePos = _imageTrans.localPosition;
            }

        }

        void OnButtonClick()
        {
            onClick.Invoke(this);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (_button.interactable && _imageTrans != null)
            {
                _imageTrans.localPosition = _defaultImagePos + _pressOffset;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_button.interactable && _imageTrans != null)
            {
                _imageTrans.localPosition = _defaultImagePos;
            }
        }
    }
}
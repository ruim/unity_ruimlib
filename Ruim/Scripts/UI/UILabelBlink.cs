﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.UI
{
    [RequireComponent(typeof(UILabel))]
    public class UILabelBlink : MonoBehaviour
    {

        public Color baseColor = Color.white;

        public float frequency;

        public bool enableBlinkOnAwake;

        public bool blinkEnabled
        {
            get
            {
                return _blinkEnabled;
            }
            set
            {
                _blinkEnabled = value;
                if (_blinkEnabled == false)
                {
                    _label.color = baseColor;
                }
            }
        }


        UILabel _label;
        float _counter;
        bool _blinkEnabled;

        private void Awake()
        {
            _label = GetComponent<UILabel>();
            if (enableBlinkOnAwake)
            {
                _blinkEnabled = true;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (blinkEnabled)
            {
                _counter += Time.unscaledDeltaTime * frequency * Mathf.PI;
                var alpha = Mathf.Abs(Mathf.Cos(_counter));
                var col = baseColor;
                col.a = alpha;
                _label.color = col;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ruim.UI
{
    public class UIRewardStar : MonoBehaviour
    {

        [SerializeField] Image _starImage;



        public Color color
        {
            set
            {
                _starImage.color = value;
            }
        }

        public void Hide()
        {
            _trans.localScale = new Vector3();
            _showProgress = 1f;
        }

        public void Show(float delay)
        {
            _showDelay = delay;
            _showProgress = 0f;
        }


        Transform _trans;
        Vector3 _iniScale;
        float _showDelay, _showProgress;

        // Start is called before the first frame update
        void Awake()
        {
            _trans = transform;
            _iniScale = _trans.localScale;
            _showProgress = 1f;
        }


        private void Update()
        {
            if (_showProgress < 1f)
            {
                if (_showDelay > 0)
                {
                    _showDelay -= Time.unscaledDeltaTime;
                    return;
                }
                _showProgress += Time.unscaledDeltaTime * 2f;
                if (_showProgress > 1f)
                {
                    _showProgress = 1f;
                }
                _trans.localScale = Vector3.Lerp(_trans.localScale, _iniScale, _showProgress);
            }
        }

    }
}
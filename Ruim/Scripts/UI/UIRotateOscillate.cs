using Ruim.Math;
using UnityEngine;

namespace Ruim.UI
{
    public class UIRotateOscillate : MonoBehaviour
    {
        [SerializeField] Angle _angle;
        [SerializeField] float _frequency;

        [SerializeField] float _minTimeScale;

        public void Reset()
        {
            _counter = 0f;
        }

        Transform _trans;
        float _counter;

        private void Awake()
        {
            _trans = transform;
        }

        private void Update()
        {
            _counter += GetDeltaTime();
            var currAngle = _angle * Mathf.Sin(_counter * Mathf.PI * _frequency);
            _trans.localRotation = Quaternion.Euler(0, 0, currAngle.degrees);

        }

        float GetDeltaTime()
        {
            var unscaled = Time.unscaledDeltaTime;
            var timeScale = Mathf.Max(_minTimeScale, Time.timeScale);
            return unscaled * timeScale;
        }
    }
}
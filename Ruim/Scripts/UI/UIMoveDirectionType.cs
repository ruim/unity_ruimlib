﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.UI
{
    public enum UIMoveDirectionType
    {
        Up, Down, Left, Right
    }
}
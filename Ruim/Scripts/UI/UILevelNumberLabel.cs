﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Ruim.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class UILevelNumberLabel : MonoBehaviour
    {

        [SerializeField] Text _label;
        [SerializeField] Image _background;

        public Color color
        {
            set
            {
                _label.color = value;
            }
        }

        public void Init(int levelNumber)
        {
            _label.text = levelNumber.ToString();

            var textWidth = LayoutUtility.GetPreferredWidth(_label.rectTransform);
            var bgTrans = _background.rectTransform;
            var bgSize = bgTrans.sizeDelta;
            var padding = 25f;
            if (textWidth > bgSize.x - padding)
            {
                bgSize.x = textWidth + padding;
                bgTrans.sizeDelta = bgSize;
            }
        }

        RectTransform _rectTrans;

        // Use this for initialization
        void Awake()
        {
            _rectTrans = GetComponent<RectTransform>();
        }
    }
}
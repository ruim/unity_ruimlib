﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI
{
    public abstract class State : MonoBehaviour
    {
        public virtual void ExecuteState() { }
        public virtual void FixedExecuteState() { }
        public virtual void OnEnterState() { }
        public virtual void OnExitState() { }
    }
}
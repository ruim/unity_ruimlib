﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Ruim.AI
{
    public class FSM : MonoBehaviour
    {
        public State activeState { get; private set; }

        public State globalState { get; private set; }

        public State prevState { get; private set; }


        private Dictionary<Type, State> m_typeToStates = new Dictionary<Type, State>();

        private void Start()
        {
            State[] states = GetComponents<State>();
            foreach (State state in states)
            {
                Type type = state.GetType();
                m_typeToStates.Add(type, state);
            }
        }

        public void SetActiveState<T>() where T : State
        {
            Type type = typeof(T);
            T newState = (T)m_typeToStates[type];

            if (activeState == newState)
            {
                return;
            }
            if (activeState != null)
            {
                activeState.OnExitState();
            }
            prevState = activeState;
            activeState = newState;
            activeState.OnEnterState();
        }


        public void SetActiveGlobalState<T>() where T : State
        {
            Type type = typeof(T);
            State state = m_typeToStates[type];
            if (globalState == state)
            {
                return;
            }
            if (globalState != null)
            {
                globalState.OnExitState();
            }
            globalState = state;
            globalState.OnEnterState();
        }

        public void SetNoState()
        {
            if (activeState != null)
            {
                activeState.OnExitState();
                activeState = null;
            }
        }

        public void SetNoGlobalState()
        {
            if (globalState != null)
            {
                globalState.OnExitState();
                globalState = null;
            }
        }

        public bool HasActiveState<T>() where T : State
        {
            if (activeState == null)
            {
                return false;
            }
            return typeof(T) == activeState.GetType();
        }

        public bool HasActiveGlobalState<T>() where T : State
        {
            if (globalState == null)
            {
                return false;
            }
            return typeof(T) == globalState.GetType();
        }

        public T GetState<T>() where T : State
        {
            Type type = typeof(T);
            return (T)m_typeToStates[type];
        }

        void Update()
        {
            if (globalState != null)
            {
                globalState.ExecuteState();
            }
            if (activeState != null)
            {
                activeState.ExecuteState();
            }
        }

        void FixedUpdate()
        {
            if (globalState != null)
            {
                globalState.FixedExecuteState();
            }
            if (activeState != null)
            {
                activeState.FixedExecuteState();
            }
        }
    }
}
﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class Arrive : SteeringBehaviour
    {
        public Transform target;

        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.Arrive(target.position, vehicle);
        }
    }

}
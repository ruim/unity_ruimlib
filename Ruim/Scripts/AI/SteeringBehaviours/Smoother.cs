﻿using UnityEngine;
using System.Collections.Generic;

namespace Ruim.AI.SteeringBehaviours
{
    public class Smoother
    {

        private List<Vector3> m_history;
        int currSample;

        public Smoother(int numSamples, Vector3 initialVelocity)
        {
            m_history = new List<Vector3>();
            for (int i = 0; i < numSamples; i++)
            {
                m_history.Add(initialVelocity);
            }
            currSample = 0;
        }

        public Vector3 UpdateSmooth(Vector3 currVelocity)
        {
            m_history[currSample] = currVelocity;
            currSample++;
            if (currSample >= m_history.Count)
            {
                currSample = 0;
            }

            Vector3 average = Vector3.zero;
            for (int i = 0; i < m_history.Count; i++)
            {
                average += m_history[i];
            }
            return average / (float)m_history.Count;
        }

    }
}
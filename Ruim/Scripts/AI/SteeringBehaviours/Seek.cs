﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class Seek : SteeringBehaviour
    {
        public Transform target;

        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.Seek(target.position, vehicle);
        }
    }
}
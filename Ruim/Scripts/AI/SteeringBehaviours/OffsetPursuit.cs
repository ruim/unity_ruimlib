﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class OffsetPursuit : SteeringBehaviour
    {
        public Vehicle leader;

        public Vector3 localOffset;

        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.OffsetPursuit(leader, localOffset, vehicle);
        }

    }
}
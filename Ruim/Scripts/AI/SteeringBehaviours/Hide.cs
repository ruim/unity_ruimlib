﻿using UnityEngine;
using System.Collections.Generic;

namespace Ruim.AI.SteeringBehaviours
{
    public class Hide : SteeringBehaviour
    {

        public Vehicle target;
        public List<Collider> obstacles;
        public float hideDistance = 5f;

        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.Hide(target, obstacles, hideDistance, vehicle);
        }
    }
}
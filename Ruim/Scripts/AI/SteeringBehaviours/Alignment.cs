﻿using UnityEngine;
using System.Collections.Generic;

namespace Ruim.AI.SteeringBehaviours
{
    public class Alignment : GroupSteeringBehaviour
    {
        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.Alignment(neighbours, vehicle);
        }
    }
}
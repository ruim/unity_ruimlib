﻿using UnityEngine;
using System.Collections.Generic;

namespace Ruim.AI.SteeringBehaviours
{
    public class FollowPath : SteeringBehaviour
    {

        public List<Transform> path;
        public int currIndex = 0;
        public float arriveRadius = 3f;
        public bool loopPath = true;

        public override Vector3 Calculate(Vehicle vehicle)
        {
            if (path.Count > 0)
            {
                return SteeringHelper.FollowPath(path, arriveRadius, loopPath, ref currIndex, vehicle);
            }
            return Vector3.zero;
        }
    }
}
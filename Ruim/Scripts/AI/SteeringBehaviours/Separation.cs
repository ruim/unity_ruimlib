﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class Separation : GroupSteeringBehaviour
    {

        public override Vector3 Calculate(Vehicle vehicle)
        {
            Vector3 force = Vector3.zero;
            for (int i = 0; i < neighbours.Count; i++)
            {
                Vehicle other = neighbours[i];
                Vector3 toOther = vehicle.position - other.position;
                force += toOther.normalized / toOther.magnitude;
            }
            return force;
        }
    }
}
﻿using UnityEngine;
using System.Collections.Generic;

namespace Ruim.AI.SteeringBehaviours
{
    [RequireComponent(typeof(SphereCollider))]
    public class AvoidObstacles : SteeringBehaviour
    {
        public float avoidForce = 5f;
        private float maxAvoidDistance;

        List<Collider> m_obstacles;

        void Awake()
        {
            m_obstacles = new List<Collider>();
            maxAvoidDistance = GetComponent<SphereCollider>().radius;
        }

        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.AvoidObstacles(m_obstacles, maxAvoidDistance, avoidForce, vehicle);
        }

        void OnTriggerEnter(Collider other)
        {
            m_obstacles.Add(other);
        }

        void OnTriggerExit(Collider other)
        {
            m_obstacles.Remove(other);
        }

    }
}
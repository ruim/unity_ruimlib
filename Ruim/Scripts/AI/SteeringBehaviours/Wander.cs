﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class Wander : SteeringBehaviour
    {

        public float radius = 1f;
        public float distance = 5f;
        public float jitter = 0.1f;

        private Vector3 localTarget;

        void Awake()
        {
            localTarget = transform.forward;
        }

        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.Wander(radius, distance, jitter, ref localTarget, vehicle);
        }

        public void OnDrawGizmos()
        {
            Vehicle vehicle = GetComponent<Vehicle>();
            if (vehicle == null)
            {
                return;
            }
            //convert to worldSpace
            Vector3 worldTarget = vehicle.LocalToWorld(localTarget);
            //move in front of the vehicle using the direction
            worldTarget += vehicle.direction * distance;

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(worldTarget, 0.5f);

            Gizmos.color = Color.gray;
            Gizmos.DrawWireSphere(vehicle.position + vehicle.direction * distance, radius);
        }

    }
}
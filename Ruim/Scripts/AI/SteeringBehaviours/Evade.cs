﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class Evade : SteeringBehaviour
    {
        public Vehicle pursuer;
        public float panicDistance = -1f;

        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.Evade(pursuer, panicDistance, vehicle);
        }


    }
}
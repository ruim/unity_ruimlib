﻿using UnityEngine;
using System.Collections.Generic;

//a Vehicle represent an object that is steered by SteeringBehaviours
//add SteeringBehaviour components to this GameObject to control it've movement

namespace Ruim.AI.SteeringBehaviours
{
    [RequireComponent(typeof(Rigidbody))]
    public class Vehicle : MonoBehaviour
    {
        public enum CombiningMode
        {
            Weight, Priority
        }

        public float maxSpeed = 10f;
        public float maxSteeringForce = 10f;
        public float deceleration = 0.3f;
        public CombiningMode combiningMode = CombiningMode.Weight;

        public bool smoothDirection = true;

        public bool loadBehavioursOnStart = true;

        public Vector3 direction { get; private set; }

        public Vector3 position => transform.position;

        public Vector3 velocity => rb.velocity;

        public Rigidbody rb { get; private set; }

        public List<SteeringBehaviour> behaviours;

        private bool m_needsSorting;

        private Smoother m_directionSmoother;

        void Start()
        {
            rb = GetComponent<Rigidbody>();
            direction = transform.forward;
            m_directionSmoother = new Smoother(20, direction);
            if (loadBehavioursOnStart)
            {
                GetComponents(behaviours);
            }
            m_needsSorting = true;
        }

        void FixedUpdate()
        {
            Vector3 force = Vector3.zero;
            switch (combiningMode)
            {
                case CombiningMode.Priority:
                    force = CalculateWithPriority();
                    break;
                case CombiningMode.Weight:
                    force = CalculateWithWeight();
                    break;
            }

            TruncateVec(ref force, maxSteeringForce);
            rb.AddForce(force, ForceMode.Impulse);

            if (rb.velocity.sqrMagnitude > maxSpeed * maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }

            if (smoothDirection)
            {
                Vector3 smoothVel = m_directionSmoother.UpdateSmooth(rb.velocity);
                if (smoothVel.sqrMagnitude > 0.1f)
                {
                    direction = smoothVel.normalized;
                }
            }
            else
            {
                if (rb.velocity.sqrMagnitude > 0.1f)
                {
                    direction = rb.velocity.normalized;
                }
            }
        }

        private bool TruncateVec(ref Vector3 v, float maxLen)
        {
            if (v.sqrMagnitude > maxLen * maxLen)
            {
                v.Normalize();
                v *= maxLen;
                return true;
            }
            return false;
        }

        private Vector3 CalculateWithPriority()
        {
            if (m_needsSorting)
            {
                SortBehaviours();
            }
            Vector3 totalForce = new Vector3();
            for (int i = 0; i < behaviours.Count; i++)
            {
                SteeringBehaviour b = behaviours[i];
                if (b.enabled)
                {
                    Vector3 force = b.Calculate(this) * b.weight;
                    if (AccumulateForce(force, ref totalForce) == false)
                    {
                        return totalForce;
                    }
                }
            }
            return totalForce;
        }

        private Vector3 CalculateWithWeight()
        {
            Vector3 force = Vector3.zero;
            for (int i = 0; i < behaviours.Count; i++)
            {
                SteeringBehaviour b = behaviours[i];
                if (b.enabled)
                {
                    force += b.Calculate(this) * b.weight;
                }
            }
            return force;
        }

        public Vector3 LocalToWorld(Vector3 localPosition)
        {
            Quaternion q = Quaternion.LookRotation(direction);
            Matrix4x4 matrix = Matrix4x4.TRS(position, q, Vector3.one);
            return matrix.MultiplyPoint(localPosition);
        }



        private void SortBehaviours()
        {
            behaviours.Sort((a, b) => -a.priority.CompareTo(b.priority));
            m_needsSorting = false;
        }


        private bool AccumulateForce(Vector3 force, ref Vector3 totalForce)
        {
            float currMagnitude = totalForce.magnitude;
            float remainingMagnitue = maxSteeringForce - currMagnitude;
            if (remainingMagnitue <= 0f)
            {
                return false;
            }
            float magnitudeToAdd = force.magnitude;
            if (magnitudeToAdd < remainingMagnitue)
            {
                totalForce += force;
            }
            else
            {
                totalForce += force.normalized * remainingMagnitue;
            }
            return true;
        }
    }
}
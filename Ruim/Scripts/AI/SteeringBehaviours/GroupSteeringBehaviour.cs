﻿using UnityEngine;
using System.Collections.Generic;

namespace Ruim.AI.SteeringBehaviours
{
    [RequireComponent(typeof(SphereCollider))]
    public abstract class GroupSteeringBehaviour : SteeringBehaviour
    {
        protected SphereCollider m_radar;

        protected List<Vehicle> neighbours { get; private set; }

        void Awake()
        {
            neighbours = new List<Vehicle>();
            m_radar = GetComponent<SphereCollider>();
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == gameObject.tag)
            {
                Vehicle otherVehicle = other.GetComponent<Vehicle>();
                neighbours.Add(otherVehicle);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == gameObject.tag)
            {
                Vehicle otherVehicle = other.GetComponent<Vehicle>();
                neighbours.Remove(otherVehicle);
            }
        }

    }
}
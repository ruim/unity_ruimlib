﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    [RequireComponent(typeof(SphereCollider))]
    public class Flocking : GroupSteeringBehaviour
    {
        public float cohesionWeight = 1f;
        public float separationWeight = 1f;
        public float alignmentWeight = 1f;

        void Awake()
        {
            m_radar = GetComponent<SphereCollider>();
        }

        public override Vector3 Calculate(Vehicle vehicle)
        {
            if (neighbours.Count == 0)
            {
                return Vector3.zero;
            }
            Vector3 force = Vector3.zero;
            force += SteeringHelper.Alignment(neighbours, vehicle) * alignmentWeight;
            force += SteeringHelper.Cohesion(neighbours, vehicle) * cohesionWeight;
            force += SteeringHelper.Separation(neighbours, vehicle) * separationWeight;
            return force;
        }
    }
}
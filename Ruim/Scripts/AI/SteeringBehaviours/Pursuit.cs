﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class Pursuit : SteeringBehaviour
    {
        public Vehicle evader;

        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.Pursuit(evader, vehicle);
        }

        void OnDrawGizmos()
        {
            if (evader == null)
            {
                return;
            }
            Vehicle vehicle = GetComponent<Vehicle>();
            if (vehicle == null)
            {
                return;
            }
            Vector3 toEvader = evader.position - vehicle.position;

            //direction factor using Dot product
            float relativeDir = Vector3.Dot(vehicle.direction, evader.direction);

            if (Vector3.Dot(toEvader, vehicle.direction) > 0f && relativeDir < -0.95f)
            {
                return;
            }

            //lookAheadTime = the distance divided by the velocities
            float lookAheadTime = toEvader.magnitude / (vehicle.maxSpeed + evader.velocity.magnitude);

            Vector3 target = evader.position + evader.velocity * lookAheadTime;

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(target, 0.5f);
            Gizmos.color = Color.gray;
            Gizmos.DrawLine(evader.position, target);
        }


    }
}
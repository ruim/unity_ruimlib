﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class Cohesion : GroupSteeringBehaviour
    {

        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.Cohesion(neighbours, vehicle);
        }
    }
}
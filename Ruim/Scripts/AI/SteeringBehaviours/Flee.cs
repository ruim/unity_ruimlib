﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class Flee : SteeringBehaviour
    {
        public Transform target;
        public float panicDistance = -1f;

        public override Vector3 Calculate(Vehicle vehicle)
        {
            if (panicDistance > 0)
            {
                return SteeringHelper.Flee(target.position, panicDistance, vehicle);
            }
            return SteeringHelper.Flee(target.position, vehicle);
        }
    }
}
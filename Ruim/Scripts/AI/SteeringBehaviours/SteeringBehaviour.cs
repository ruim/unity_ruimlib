﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    [RequireComponent(typeof(Vehicle))]
    public abstract class SteeringBehaviour : MonoBehaviour
    {
        public float weight = 1f;
        public int priority = 0;

        public abstract Vector3 Calculate(Vehicle vehicle);
    }
}
﻿using UnityEngine;
using System.Collections;

namespace Ruim.AI.SteeringBehaviours
{
    public class Interpose : SteeringBehaviour
    {

        public Vehicle agentA;
        public Vehicle agentB;


        public override Vector3 Calculate(Vehicle vehicle)
        {
            return SteeringHelper.Interpose(agentA, agentB, vehicle);
        }
    }
}
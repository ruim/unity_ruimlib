﻿using UnityEngine;
using System.Collections.Generic;

namespace Ruim.AI.SteeringBehaviours
{
    public static class SteeringHelper
    {

        public static Vector3 Seek(Vector3 target, Vehicle vehicle)
        {
            Vector3 desired = (target - vehicle.position).normalized * vehicle.maxSpeed;
            return desired - vehicle.velocity;
        }

        public static Vector3 Flee(Vector3 target, Vehicle vehicle)
        {
            Vector3 desired = (vehicle.position - target).normalized * vehicle.maxSpeed;
            return desired - vehicle.velocity;
        }

        public static Vector3 Flee(Vector3 target, float panicDistance, Vehicle vehicle)
        {
            Vector3 fromTarget = vehicle.position - target;
            if (fromTarget.sqrMagnitude < panicDistance * panicDistance)
            {
                return Flee(target, vehicle);
            }
            return Vector3.zero;
        }

        public static Vector3 Arrive(Vector3 target, Vehicle vehicle)
        {
            Vector3 toTarget = target - vehicle.position;
            float dist = toTarget.magnitude;
            if (dist > 0f)
            {
                float speed = dist / vehicle.deceleration;
                speed = Mathf.Min(speed, vehicle.maxSpeed);

                Vector3 desired = toTarget * speed / dist;
                return desired - vehicle.velocity;
            }
            return Vector3.zero;
        }

        public static Vector3 Pursuit(Vehicle evader, Vehicle vehicle)
        {
            Vector3 toEvader = evader.position - vehicle.position;

            //direction factor using Dot product
            float relativeDir = Vector3.Dot(vehicle.direction, evader.direction);

            //if dot is less than 20degrees: ACos(18º) = -0.95f
            if (Vector3.Dot(toEvader, vehicle.direction) > 0f && relativeDir < -0.95f)
            {
                //just Seek the target
                return Seek(evader.position, vehicle);
            }

            //lookAheadTime = the distance divided by the velocities
            float lookAheadTime = toEvader.magnitude / (vehicle.maxSpeed + evader.velocity.magnitude);
            return Seek(evader.position + evader.velocity * lookAheadTime, vehicle);
        }

        public static Vector3 Evade(Vehicle pursuer, float panicDistance, Vehicle vehicle)
        {
            Vector3 toPursuer = pursuer.position - vehicle.position;
            float lookAheadTime = toPursuer.magnitude / (pursuer.velocity.magnitude + vehicle.maxSpeed);
            if (panicDistance > 0)
            {
                return Flee(pursuer.position + pursuer.velocity * lookAheadTime, panicDistance, vehicle);
            }
            return Flee(pursuer.position + pursuer.velocity * lookAheadTime, vehicle);
        }

        public static Vector3 Interpose(Vehicle agentA, Vehicle agentB, Vehicle vehicle)
        {
            Vector3 midPoint = (agentA.position + agentB.position) * 0.5f;
            float timeToReachMidpoint = (vehicle.position - midPoint).magnitude / vehicle.maxSpeed;
            Vector3 posA = agentA.position + agentA.velocity * timeToReachMidpoint;
            Vector3 posB = agentB.position + agentB.velocity * timeToReachMidpoint;

            midPoint = (posA + posB) * 0.5f;

            return Arrive(midPoint, vehicle);
        }

        public static Vector3 Hide(Vehicle target, List<Collider> obstacles, float hideDistance, Vehicle vehicle)
        {
            Vector3 bestHidePos = Vector3.zero;
            float minDist = float.MaxValue;
            //calculate nearest hiding position
            for (int i = 0; i < obstacles.Count; i++)
            {
                Vector3 hidePos = GetHidingPos(obstacles[i], hideDistance, target.position, vehicle);
                float dist = (hidePos - vehicle.position).magnitude;
                if (dist < minDist)
                {
                    bestHidePos = hidePos;
                    minDist = dist;
                }
            }
            //if no hiding position was found, then Evade the target
            if (minDist == float.MaxValue)
            {
                return Evade(target, -1f, vehicle);
            }
            //return Arrive to the hiding position
            return Arrive(bestHidePos, vehicle);
        }

        public static Vector3 GetHidingPos(Collider obstacle, float hideDistance, Vector3 targetPos, Vehicle vehicle)
        {
            //nearest position inside the obstacle's bounding box to the vehicle
            //Vector3 obstaclePos = obstacle.bounds.ClosestPoint (vehicle.position);
            Vector3 obstaclePos = obstacle.bounds.center;
            //calc direction between obstacle and enemy
            Vector3 toEnemy = obstaclePos - targetPos;
            //hidign position
            return obstaclePos + toEnemy.normalized * hideDistance;
        }

        public static Vector3 OffsetPursuit(Vehicle leader, Vector3 localOffset, Vehicle vehicle)
        {
            Vector3 worldPos = leader.LocalToWorld(localOffset);
            Vector3 toOffset = worldPos - vehicle.position;
            float lookAheadTime = toOffset.magnitude / (leader.velocity.magnitude + vehicle.maxSpeed);
            return Arrive(worldPos + leader.direction * lookAheadTime, vehicle);
        }

        public static Vector3 FollowPath(List<Transform> path, float arriveRadius, bool loopPath, ref int currIndex, Vehicle vehicle)
        {
            //current waypoint
            Transform wayPoint = path[currIndex];

            //calc difference to wayPoint
            Vector3 toWayPoint = wayPoint.position - vehicle.position;

            //is this the last point? (can only be true is loop is set to false)
            bool lastPoint = false;

            //if distance is less than arriveRadius then we must calc the next way point
            if (toWayPoint.sqrMagnitude <= arriveRadius * arriveRadius)
            {
                //if next waypoint exceeds the amount of waypoint what do we do??
                if (currIndex + 1 >= path.Count)
                {
                    //if loop, then set to zero
                    if (loopPath)
                    {
                        currIndex = 0;
                    }
                    else
                    {
                        //otherwise mark this as the last point
                        lastPoint = true;
                    }
                }
                else
                {
                    //if this is not the last waypoint, so increment it
                    currIndex++;
                }
            }
            //if it is the last point then Arrive at it
            if (lastPoint)
            {
                return Arrive(wayPoint.position, vehicle);
            }
            //if not, then Seek it
            return Seek(wayPoint.position, vehicle);
        }

        public static Vector3 Wander(float radius, float distance, float jitter, ref Vector3 localTarget, Vehicle vehicle)
        {
            //move localTarget by a random amount (jitter)
            localTarget += new Vector3(Random.Range(-jitter, jitter), Random.Range(-jitter, jitter), Random.Range(-jitter, jitter));
            //normalize localTarget and mult by radius to keep in sphere
            localTarget.Normalize();
            localTarget *= radius;

            //convert to worldSpace
            Vector3 worldTarget = vehicle.LocalToWorld(localTarget);
            //move in front of the vehicle using the direction
            worldTarget += vehicle.direction * distance;

            //return the difference
            return worldTarget - vehicle.position;
        }

        public static Vector3 AvoidObstacles(List<Collider> obstacles, float maxAvoidDistance, float avoidForce, Vehicle vehicle)
        {
            if (obstacles.Count == 0)
            {
                return Vector3.zero;
            }

            //Accumulator for repulsion of all detected obstacles
            Vector3 repulsion = Vector3.zero;
            for (int i = 0; i < obstacles.Count; i++)
            {
                Collider obstacle = obstacles[i];
                //direction vector
                Vector3 oposite = vehicle.position - obstacle.bounds.center;
                if (oposite.sqrMagnitude <= maxAvoidDistance * maxAvoidDistance)
                {

                    //mul = normalized value between 0..1 of the distance to the obstacle
                    //used to make the repulsion smaller if further away
                    float mul = 1f - oposite.magnitude / maxAvoidDistance;
                    //add the repulsion * mul * avoidance force
                    repulsion += oposite * mul * avoidForce;
                }
            }
            //return repulsion;
            //desired velocity is the current velocity reflected by the repulsion vector
            Vector3 desiredVelocity = Vector3.Reflect(vehicle.velocity, repulsion);
            //set desired velocity to correct speed (vehicle.maxSpeed)
            desiredVelocity = desiredVelocity.normalized * vehicle.maxSpeed;
            return desiredVelocity - vehicle.velocity;
        }

        public static Vector3 Cohesion(List<Vehicle> neighbours, Vehicle vehicle)
        {
            if (neighbours.Count == 0)
            {
                return Vector3.zero;
            }
            Vector3 centerOfMass = Vector3.zero;
            for (int i = 0; i < neighbours.Count; i++)
            {
                Vehicle other = neighbours[i];
                centerOfMass += other.position;
            }
            centerOfMass /= (float)neighbours.Count;
            return Seek(centerOfMass, vehicle);
        }

        public static Vector3 Alignment(List<Vehicle> neighbours, Vehicle vehicle)
        {
            if (neighbours.Count == 0)
            {
                return Vector3.zero;
            }
            Vector3 averageVelocity = Vector3.zero;
            for (int i = 0; i < neighbours.Count; i++)
            {
                Vehicle other = neighbours[i];
                averageVelocity += other.velocity;
            }
            averageVelocity /= (float)neighbours.Count;
            return averageVelocity - vehicle.velocity;
        }

        public static Vector3 Separation(List<Vehicle> neighbours, Vehicle vehicle)
        {
            Vector3 force = Vector3.zero;
            for (int i = 0; i < neighbours.Count; i++)
            {
                Vehicle other = neighbours[i];
                Vector3 toOther = vehicle.position - other.position;
                force += toOther.normalized / toOther.magnitude;
            }
            return force;
        }


    }
}